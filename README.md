# GSoC_Code_Challenge
Code Challenge for GSoC'21 

### Project: Pathology Smartpens

##### Code Challenge: Create a page or tool which performs edge detection on a given image and, given a point, returns the distance from that point to the closest edge.


#
#
#### About Image Processing Library Created:
A Library is created in python which performs tasks like:
- Applying Median Filter
- Applying Kernals
- Calculating Shortest Distance
- Non-Max Suppression
- Thresholding

[Code](https://bitbucket.org/chaitravi-ce/gsoc_code_challenge_/src/main/ProLib.py)

### Results

[Original Image](https://bitbucket.org/chaitravi-ce/gsoc_code_challenge_/src/main/brainMRI.png) 

[Image after Sobel Edge Detection](https://bitbucket.org/chaitravi-ce/gsoc_code_challenge_/src/main/sobel.jpg)

[Image after Canny Edge Detection](https://bitbucket.org/chaitravi-ce/gsoc_code_challenge_/src/main/canny.jpg)

[Image showing the shortest distance of a point from a point on edge](https://bitbucket.org/chaitravi-ce/gsoc_code_challenge_/src/main/LineDrawn.png)
